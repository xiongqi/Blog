---
title: 个人博客记 —— Github pages 绑定个人域名
cdn: header-off
date: 2017-09-16
header-img: /images/articles/github-domain/bg.png
description: 个人博客记 —— Github pages 绑定个人域名 - xqone blog
keywords: xqone blog domain 绑定域名
sid: 2017091601
tags:
  - 原创
  - github
  - blog
---

前言
==
近日搭建了自己的 `Blog`，使用的是 `Hexo` + `github pages`。众所周知，`github pages`的域名都是统一的`your_name.github.io`。想了想，能不能换个域名呢？当然是可以滴。  
于是，在度娘了之后，也算找到了方法。但是网上的介绍都是操作流程，没有详细的解释，我自己也是有些迷糊。好在域名绑定成功了。遂决定写一篇博文分享一下自己对这个过程的理解。  
请自行准备：个人的 `github pages`，个人域名。  

简述
==
绑定域名需要在 **域名解析服务商** 和 **`github`** 两边都进行操作。  
1. 在域名解析服务商进行个人域名解析，将域名绑定到个人 `github pages`。  
2. 同时在 `github pages` 需要配置 `CNAME` 文件将网站重定向到你的域名。  

1.域名解析配置
==
简单点说就是将域名和IP或其他域名进行绑定，让你能通过不同域名访问同一个网站。  
解析商不多说，`DNSPod`、`阿里云`、`腾讯云`等等。一般国内域名购买商都同时提供解析服务。我使用的是`腾讯云`买的域名 `xhuyq.me` ，就用腾讯云解析了。  

**注：以下过程为通用版本，非具体解析商的操作过程。**

首先找到域名管理，选择域名解析功能。`记录类型`（如图提示），我们选择 `A记录` 或者 `CNAME` 就可以了。接下里就是域名解析的几个要点：  

![domain-1][1] ![domain-2][2] 

(1) `A记录` 类型 `记录值`填写 `IP` 值，两种选择：  
```
> 你的 `github.io` 的 `IP` 值
> 在官方提供的两个 `IP` 中选择 `192.30.252.153`、`192.30.252.154`
```
(2) `CNAME`类型 （如图提示），请选好`主机记录`，按`主机记录`类型填写记录值。推荐选择的 `@` ，那么`记录值`就填写按各位`github`名填写`your_name.github.io`就好了。  

2.`github pages` 方面的 `CNAME` 文件配置
===
（1）在`github`的`github pages`的仓库根目录里加上`CNAME`文件，里面写上个人域名即可。我的是：  
```
xhuyq.me
```
（2）或者直接在`github.io`仓库的`Settings`的`GitHub Pages`项直接设置`Custom domain`,`github`会自动添加`CNAME`文件:  

![domain-3][3]

3.个人理解
===
那么，为什么配置了域名解析还要配置`CNAME` 文件呢？  
实际上，`个人域名`是`名字`，`github pages`相当于`网站空间`。试想，如果不在`网站空间`配置`CNAME`，就可以成功绑定域名，那么岂不是可以给别随便一个的网站绑定上自己的域名？相同，如果只需配置`github pages`的`CNAME`，而不用`域名解析`，那不是也可以将自己的网站绑在别人的域名上？  
**所以，域名绑定是需要`“双方同意”`的。**  
如有不当，万望指正！  



[1]: /images/articles/github-domain/domain-1.png
[2]: /images/articles/github-domain/domain-2.png
[3]: /images/articles/github-domain/domain-3.png

[4]: http://www.jianshu.com/p/cd0b80f7f1c4
[5]: https://segmentfault.com/a/1190000011203711
