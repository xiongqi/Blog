---
title:  CSS中"position:relative"属性与文档流的确切关系
cdn: header-off
header-img: /images/articles/relative-in-flow/bg.png
description: xqone blog CSS中"position"属性与文档流的关系
tags:
  - 原创
  - html
  - css
date: 2017-09-27
sid: 2017092701
keywords: xqone blog css position relative
---

正文
==
近期遇到一个问题——`"position:relative"`到底会不会导致元素脱离文档流？主流观点是不会，但都给不出一个有说服力的论据。最后我自己佐证了一番，总算有了个结果：  
**`"position:relative"`不会导致元素脱离文档流**。  

`"relative"`与文档流
==
说到标准，最权威的自然莫过于`CSS标准文档`。经过一番繁琐的查找之后（w3c网站找东西是真的累。。。），终于被我找到了。文档中`"positioning-scheme"`一节写道：  
![relative-in-flow][1]
链接：https://www.w3.org/TR/CSS22/visuren.html#positioning-scheme  
``` bash
An element is called out of flow if it is floated, absolutely positioned, or is the root element. An element is called in-flow if it is not out-of-flow. 
```
```
当元素是浮动，绝对定位或者根元素时，元素被称为"流外元素"，否则被称为"流内元素"。
```
很明显，`"position:relative"`的元素仍在文档流中。  
另外，推荐下一个人认为不错的文章：  
[html/css基础篇——DOM中关于脱离文档流的几种情况分析][2]  


[1]: /images/articles/relative-in-flow/relative-1.png
[2]: http://www.cnblogs.com/chuaWeb/p/html_css_position_float.html 
[3]: http://www.jianshu.com/p/5dfa4d41558d
[4]: https://segmentfault.com/a/1190000011395218
