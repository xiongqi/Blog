---
title: 华为2018届校招技术岗笔试题及个人解答
subtitle: 语言：JavaScript
date: 2017-09-14
cdn: header-off
header-img: /images/articles/huawei/bg.png
keywords: xqone blog 华为 2018校招笔试
description: 华为2018届校招技术岗笔试题及个人解答 - xqone blog
sid: 2017091401
tags:
    - 原创
    - 笔试
    - JavaScript
    - node
---

前言
==
昨天（9.13）参加了华为2018届的技术岗笔试，特此总结一下笔试的题目和我个人的解答思路。  
笔试题一共是三道编程题，大致是数值反向输出，比较和排序，相对较基础。  
PS：由于没有截图，所以题目是根据我自己的记忆复述的，各位见谅`(¯﹃¯)`。   

第一题
==
题目：
```
输入一个整数（含负数），输出3个数据，如下：
1.输出该整数的位数;
2.将该整数各位拆分输出，中间以空格隔开（注意末位不能有空格）。如果是负数，则符号与第一个数一起输出；
3.输出该数的反转数，如为负数，符号位置不变，置于最前。
```
示例
输入：
``` bash
-12345
```
输出：
``` bash
5
-1 2 3 4 5
-54321
```
我的代码如下（JavaScript-Node）：

``` javascript
/*** Node输入输出模块 类似Java的Scanner ***/
var readline = require("readline");
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/* rl.on("line",function(data)) 监听输入，data为每次输入的的一行数据，每输入新行便触发一次function(data) */
rl.on("line", function (data) {
  var num_space;
  var num_reverse;
  if (data[0] == "-") {
    num_space = "-" + data.slice(1).split("").join(" "); /** 2.含空格整数 **/
    num_reverse = "-" + data.slice(1).split("").reverse().join(""); /** 3.反转整数 **/
    console.log((data.length - 1) + "\n" + num_space + "\n" + num_reverse);
  } else {
    num_space = data.split("").join(" "); /** 2.含空格整数 **/
    num_reverse = data.split("").reverse().join(""); /** 3.反转整数 **/
    console.log(data.length + "\n" + num_space + "\n" + num_reverse);
  }
});
```

第二题
==
题目：
```
输入4个IP值组成两个IP段：
第一、二行分别为第一个IP段的起始和结尾IP，第三、四行为第二个IP段的起始和结尾。
要求输出：
若两个IP段有交集则输出"Overlap IP"，没有则输出"No Overlap IP"。
```
示例
输入：
``` bash
1.1.1.1
255.255.255.255
2.2.2.2
3.3.3.3
```
输出：
``` bash
Overlap IP
```
我的代码如下（JavaScript-Node）：

``` javascript
/*** Node输入输出模块 类似Java的Scanner ***/
var readline = require("readline");
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/*** rl.on("line",function(ip)) 监听输入，ip为每次输入的一行数据，每输入新行便触发一次function(ip) ***/
var ips = new Array(0);//存储4个IP值
rl.on("line", function (ip) {
  /* IP值处理，转为易比较方式 */
  if (ips.length < 4) {   /** 存储的IP数小于4个则继续处理并存储 **/
    ips[ips.length] = ip.split(".").map(function (x) { //坑爹，笔试环境不支持es6，不能用箭头函数 
      if (3 - x.length) {
        x = (3 - x.length > 1 ? "00" : "0") + x;
      }
      return x;
    }).join("");//join("")不必需,比较时自动toString(),虽然各项之间有逗号但不影响比较结果
  }
  /** 4个IP均已获得，开始比较 **/
  if (ips.length == 4) {
    if (ips[2] > ips[1] || ips[3] < ips[0])
      console.log("No Overlap IP");
    else
      console.log("Overlap IP");
    ips = [];//清空ips，为下次输入做准备
  }
});
```

第三题
==
题目：
```
输入两行数据，第一行包含多个正整数，以空格分开，根据每个数的后三位大小进行排序；第二行为数值n,输出排序后指定位置n的数。
要求：
1.若数不足三位，则直接比较；
2.若两数比较结果相等，则两数相对位置不变。
要求输出：
排序后第n个数（位置从1开始）。
```
示例
输入：
``` bash
12 450 9001 5231 8231 7231
5
```
输出：
``` bash
7231
```

我的代码如下（JavaScript-Node）：

``` javascript
/*** Node输入输出模块 类似Java的Scanner ***/
var readline = require("readline");
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/* rl.on("line",function(data)) 监听输入，data为每次输入的一行数据，每输入新行便触发一次function(data) */
var nums = new Array(0);
rl.on("line", function (data) {
  if (!nums.length) {
    nums = data.split(" ").sort(function (x1, x2) {
      if (x1.length > 2)
        x1 = x1.slice(-3); //截取从倒数第三位到结尾的字符串
      if (x2.length > 2)
        x2 = x2.slice(-3);
      return x1 - x2; //返回负数则x1排在x2前面
    });
  }
  else {
    console.log(nums[data - 1]);
    nums = []; //清空nums
  }
});
```
总结
==
华为的笔试编程题总体来说比较基础（或者我分到的题目比较简单？哈哈），主要是排序、比较类的题目。看来华为还是很给我们面子的哈哈`<(￣︶￣)>`。  
另外，我用的是 `JavaScript` ，因为题目比较简单，好理解，所有没有太多注释，其他语言的同学欢迎参考，语言都是相通的。  
