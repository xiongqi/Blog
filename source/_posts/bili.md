---
title: 哔哩哔哩2018校招前端笔试
cdn: header-off
header-img: /images/articles/bili/bg.png
description: xqone blog bilibili 笔试
tags:
    - 原创
    - 笔试
    - JavaScript
date: 2017-10-13
sid: 2017101301
keywords: xqone blog bilibili 笔试
---

# 前言
前几日，哔哩哔哩在电子科大清水河校区举行了校招宣讲会。B站不用多说，中国最大的同性交友网站:smile:，不去工作也能去看看。  
当晚便进行了部分笔试，下面是我参加的前端笔试试题和我自己的解答，分享给大家。  

## 第一题

```
给定一个整数数组，找到具有最大和的子数组，返回最大和。
```
比较常见，直接给我的解答：
```js
// es6
function (arr) {

	//全为负数的情况，要注意
    if (arr.every(x => x < 0)) {
        return arr.sort()[0];
    }

    var max = 0;
    var sum = 0;
    for (let i of arr) {
        sum += i;
        if (max < sum)
            max = sum;
        if (sum < 0)
            sum = 0;
    }
    return max;
```
## 第二题
```
要求:
1. 构建一个n*n的格子(n从输入框填写) ,默认背景色都是白色,鼠标指针指上去的格子底色要变
成红色,鼠标移出时复原
2. 鼠标点击格子时背景色固定为蓝色,指针hover时也不变红。再次点击时复原成未点击的状态
3. 格子大小可用css控制,底色变化用js实现
```
个人思路如下：
### 1. 创建n*n的表格
```html
<!-- html部分 -->
<input id="in" type="text" placeholder="输入n">
<button onclick="createTable()">确认n</button>
<br><br>

<!-- css -->
<style>
td{
	width:20px;
	height:20px;
	border:1px solid;		
}
</style>

<!-- js -->
<script>
// 创建n*n表格
function createTable(){
	var n=document.getElementById("in").value;

	//创建n*n的"table"
	var str="<tbody>";
	for(var i=0;i<n;i++){
		str+="<tr>";
		for(var j=0;j<n;j++){
			str+="<td></td>";
		}
		str+="</tr>";
	}
	str += "</tbody>";
	var table = document.createElement("table");
	table.innerHTML = str;

	table.setAttribute("cellspacing", 0);//消除表格间的间距
	document.body.appendChild(table);

	//调用事件函数，传入表格
	colorEvent(table);
}
</script>
```
### 2. 颜色变化效果
使用事件委托。
```html
<script>
//mouseover与mouseout
//不论鼠标指针穿过被选元素或其子元素，都会触发与mouseout。

//mouseenter与mouseleave
//只有在鼠标指针离开被选元素时，才会触发与mouseout。

function colorEvent(table) {	
	//指针hover，out以及click监听
	table.addEventListener("mouseover", colorHover, false);
	table.addEventListener("mouseout", colorOut, false);
	table.addEventListener("click", colorClick, false);

	function colorHover(ev) {
		var e = ev || window.event;
		var target = e.target || e.srcElement;
		if (target.tagName == "TD" && target.style.background != "blue") {
			target.style.background = "red"; //注意"tagName"为大写，写小写"td"会失败
		}
	}
	function colorOut(ev) {
		var e = ev || window.event;
		var target = e.target || e.srcElement;
		if (target.tagName == "TD" && target.style.background != "blue") {
			target.style.background = "white";
		}
	}
	function colorClick(ev) {
		var e = ev || window.event;
		var target = e.target || e.srcElement;
		if (target.tagName == "TD") {
			target.style.background = (target.style.background == "blue" ? "white" : "blue");
		}
	}
}
</script>
```
其实此题如果使用`jQuery`会比较方便，其大致思路如下：
```js
$(document).ready(function(){
	$("#t").hover(function(){
		$("#t").css("background-color","red");
	},function(){
		$("#t").css("background-color","white");
	});
});
```
### 3. 源代码
[哔哩哔哩笔试第三题.html][1]
## 第三题
此题为还原一张图上的设计，此处无图就不叙述了。
## 第四题
题目：高铁的玻璃需要有一定的防撞击强度,可以通过不同时速的铝弹撞击玻璃实际测试,可测出不会被击穿的最大时速。现在有两块强度未知的玻璃需要实验验证防撞击最大时速,假设不会被击穿时玻璃都完好无损可以继续实验使用, 一旦被击穿则不能继续使用。如何通过这两块玻璃,用尽量少的实验次数验证出防撞击的最大时速。(可以有微小误差,两块玻璃都可以被击穿)  
题有点长，我的思路是使用类似于计算机网络拥塞控制的方法，使用`先指数增后线性增长`的方法。  
（使用1,2,3表示时速等级，等级越高，时速越快）
1. 使用 `2^n` 级时速依次测试玻璃1（n=0,1,2...）。
2. 假设 `2^m` 级时速时穿过玻璃1，记录下当前的时速间隔：( 2^(m-1),2^m )。
3. 从 `2^(m-1)` 开始依次加1级测试玻璃2，直至测出最大时速。  

[1]: https://github.com/XQONE/xqone.github.io/blob/master/resources/哔哩哔哩笔试第三题.html.code  
[2]: http://www.jianshu.com/p/102c61d56cd5
[3]: https://segmentfault.com/a/1190000011690838
